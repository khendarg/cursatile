import curses

class ZeroDimException(BaseException): pass

class Tile(object):
	def __init__(self, screen, *args, **kwargs):
		self.screen = screen
		if "children" in kwargs:
			self.children = kwargs.pop("children")

		self.args = args
		self.kwargs = kwargs

		self.forbid_zero_dim = kwargs.get("forbid_zero_dim", True)

		if not kwargs.get("manual"): self.run()

	def run(self):
		pass

	def split(self, oldtileheight=None, newtileheight=None):
		curdim = self.screen.getmaxyx()
		if oldtileheight is None and newtileheight is None:
			newoldtileheight = self.sc

class Float(object):
	def __init__(self, screen, *args, **kwargs):
		self.position = kwargs.get("position")
		self.size = kwargs.get("size")

